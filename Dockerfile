FROM ruby:2.1.8
MAINTAINER Héctor Ramón <hector0193@gmail.com>

RUN apt-get update
RUN apt-get install -y postgresql postgresql-contrib libpq-dev redis-server libicu-dev cmake g++ nodejs libkrb5-dev ed pkg-config golang

ENV PHANTOM_JS="phantomjs-1.9.8-linux-x86_64"
RUN wget https://bitbucket.org/ariya/phantomjs/downloads/$PHANTOM_JS.tar.bz2
RUN tar -xvjf $PHANTOM_JS.tar.bz2
RUN mv $PHANTOM_JS /usr/local/share
RUN ln -s /usr/local/share/$PHANTOM_JS/bin/phantomjs /usr/local/bin
RUN phantomjs --version

RUN useradd -ms /bin/bash gitlab
USER gitlab
WORKDIR /home/gitlab

RUN git clone https://gitlab.com/gitlab-org/gitlab-development-kit.git

WORKDIR /home/gitlab/gitlab-development-kit

RUN gem install bundler
RUN make gitlab_repo=https://gitlab.com/hecrj/gitlab-ce.git
ADD start.sh start.sh
CMD ./start.sh

EXPOSE 3000
